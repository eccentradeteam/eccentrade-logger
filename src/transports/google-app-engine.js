import winston from 'winston';
import util from 'util';
import os from 'os';
import fs from 'fs';

const levels = {
  emergency: 0,
  alert: 1,
  critical: 2,
  error: 3,
  warning: 4,
  notice: 5,
  info: 6,
  debug: 7,
  default: 8,
};

const GoogleAppEngine = function GoogleAppEngine(options) {
  this.name = 'GoogleAppEngine';
  this.level = options.level || 'info';
};

util.inherits(GoogleAppEngine, winston.Transport);
winston.transports.GoogleAppEngine = GoogleAppEngine;
winston.config.GoogleAppEngine = {
  levels,
};

GoogleAppEngine.prototype.log = function log(level, msg, meta, callback) {
  if (this.silent) {
    return callback(null, true);
  }

  // if meta is ommitted, make sure callback is assigned correctly
  if (typeof(meta) === 'function' && !callback) {
    callback = meta;
  }

  if (typeof(msg) !== 'string') {
    msg = util.inspect(msg);
  }

  const milliseconds = (new Date()).getTime();
  const seconds = Math.floor(milliseconds / 1000);
  const nanos = (milliseconds % 1000) * 1000000;
  const severity = level.toUpperCase();

  const output = {
    timestamp: {
      seconds,
      nanos,
    },
    message: msg,
    severity,
  };

  const line = JSON.stringify(output) + os.EOL;
  fs.appendFile('/var/log/app_engine/app.log.json', line, callback);
};
