import { each } from 'lodash';
require('winston-mongodb').MongoDB;

const winston = require('winston');
const config = require('config');

const settings = config.get('logging');
const levels = {
  emergency: 0,
  alert: 1,
  critical: 2,
  error: 3,
  warning: 4,
  notice: 5,
  info: 6,
  debug: 7,
  default: 8,
};

const Logger = new winston.Logger({
  levels,
});

each(settings.methods, (method, type) => {
  if (!method.enabled) return;

  const options = settings.methods[type];
  switch (type) {
    case 'console':
      Logger.add(winston.transports.Console, options);
      break;
    case 'database':
      Logger.add(winston.transports.MongoDB, options);
      break;
    default:
      // TODO maybe implement other transporters like Slack for critical errors.
      break;
  }
  Logger.debug(`Logging enabled on ${type} (${options.level})`);
});

export default Logger;
