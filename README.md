### What is it? ###
General purpose logging for Node apps based on Winston.
It supports console and mongodb transports by for now.

### Usage ###
```
import logger from 'eccentrade-logger'
logger.error('You did something wrong!')
```

The logging output settings can be configured using node-config, i.e. in your /config/development.json:
```
  "logging": {
    "methods": {
      "console": {
        "enabled": true,
        "level": "debug",
        "prettyPrint": true,
        "colorize": true
      },
      "database": {
        "enabled": false,
        "level": "info"
      }
    }
  }
```
